
# Setting PATH for Python 2.7
# This prevents bashrc running when the session is over ssh. Snazzy command
# prompt impacts ability to clone/pull peer to peer
if [ -f ~/.bashrc ] && [ "x${SSH_TTY}" = "x" ]; then
  source ~/.bashrc
fi

if [ -f ~/.aliases.sh ]; then
  source ~/.aliases.sh;
fi

if [ -f ~/.victor_aliases.sh ]; then
  source ~/.victor_aliases.sh;
fi


# Path additions
export PATH=\
/Users/jameslees/.node/bin:\
/usr/local/bin:\
/usr/bin:\
/usr/texbin:\
/bin:\
/usr/sbin:\
/sbin:/Users/jameslees/.node/bin:\
/Library/Frameworks/Python.framework/Versions/2.7/bin:\

# Initialise rbenv
if which rbenv > /dev/null; then eval "$(rbenv init -)"; fi

# added by Anaconda3 2.4.1 installer
export PATH="/Users/jameslees/anaconda3/bin:$PATH"
