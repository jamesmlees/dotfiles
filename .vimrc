"~*~*~*~ Vundle Config ~*~*~*
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" Vundle plugins
Plugin 'gmarik/Vundle.vim'
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-surround'
Plugin 'edsono/vim-matchit'
Plugin 'tpope/vim-markdown'
Plugin 'jelera/vim-javascript-syntax'
Plugin 'flazz/vim-colorschemes'
Plugin 'elzr/vim-json'
Plugin 'moll/vim-node'
Plugin 'tomswartz07/vim-todo'
Plugin 'bling/vim-airline'
Plugin 'Shutnik/jshint2.vim'
Plugin 'christoomey/vim-tmux-navigator'
Plugin 'editorconfig/editorconfig-vim'
Plugin 'groenewege/vim-less'
Plugin 'mustache/vim-mustache-handlebars'
Plugin 'rking/ag.vim'

call vundle#end()            " required
filetype plugin indent on    " required
"~*~*~*~ End Vundle Config ~*~*~*

"Syntax highlighting on
syntax on

"Use mac clipboard
set clipboard=unnamed

"Fancy tabline
"let g:airline#extensions#tabline#enabled = 1

" Add a coloured line at 81chars
set textwidth=80
set colorcolumn=+1

"Tab spacing to two spaces
set expandtab
set tabstop=2
set softtabstop=2
set shiftwidth=2

"Ignore case in search unless upper case chars included
set smartcase
set ignorecase

" Store .swps in .vim/swps
set directory=~/.vim/swps

"Treat JSON files like JS
if has("autocmd")
    " Enable file type detection
    filetype on
    " Treat .json files as .js
    autocmd BufNewFile,BufRead *.json setfiletype json syntax=javascript
endif

"Show “invisible” characters
set lcs=tab:▸\ ,trail:·,eol:¬,nbsp:_
set list

"autoindent text
set autoindent

"show line numbers
set nu
set noerrorbells
set novisualbell

" Custom colour scheme
colorscheme softblueJ

set guitablabel=%t
set t_vb=
set tm=500

"Disable arrow keys
inoremap  <Up>     <NOP>
inoremap  <Down>   <NOP>
inoremap  <Left>   <NOP>
inoremap  <Right>  <NOP>
noremap   <Up>     <NOP>
noremap   <Down>   <NOP>
noremap   <Left>   <NOP>
noremap   <Right>  <NOP>

"Remap shift and h/l to navigate through tabs
nnoremap <S-h> gT
nnoremap <S-l> gt

set t_Co=256

set laststatus:2

"set statusline+=%t
"set statusline+=%{fugitive#statusline()}
"set statusline+=%r
"set statusline+=%m
let g:airline_powerline_fonts = 1

"Use the .vimrc file in project directory
set exrc
set secure

"use ag for ack
let g:ackprg = 'ag --nogroup --nocolor --column'
let g:netrw_liststyle=3
