# Aliases
alias gpg=gpg2 # always use gpg2.1
alias prettify='python -m json.tool'
alias pre='source ~/dev/notes-and-stuff/scripts/pre.sh'
alias pushpass='cd ~/.password-store && git push origin master && cd -'
alias pullpass='cd ~/.password-store && git pull origin master && cd -'
alias pushdotfiles='cd ~/dotfiles && git push origin master && cd -'
alias tmat='tmux attach-session -t'
alias api="~/dev/notes-and-stuff/scripts/http-requests/generic/api.sh"
alias apiStatusD='http http://api-dev.flyvictor.com/status'
alias apiStatusS='http http://api-staging.flyvictor.com/status'
alias apiStatusP='http http://api.flyvictor.com/status'

alias agg='ag'
alias ag='ag --path-to-agignore=~/.agignore'

alias mongov='mongo --host localhost:24999'
alias la='ls -al'
